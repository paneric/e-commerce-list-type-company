<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Dotenv\Dotenv;
use Monolog\Logger;
use Paneric\Logger\LoggerFactory;
use Paneric\ModuleResolver\ModuleResolverBuilder;
use Slim\App;

define('ROOT_FOLDER', dirname(__DIR__) . '/');
define('APP_FOLDER', dirname(__DIR__) . '/src/');

require ROOT_FOLDER . 'vendor/autoload.php';

$dotEnv = Dotenv::createImmutable(ROOT_FOLDER);
$dotEnv->load();
define('ENV',$_ENV['ENV']);

if (ENV !== 'dev') {
    ini_set('display_errors', '1');
    ini_set('log_errors', '0');
    ini_set('error_log', ROOT_FOLDER . 'var/logs/server.log');
    error_reporting(E_ALL);

    error_log($_SERVER['REQUEST_URI'], 0);
}

$moduleResolver = new ModuleResolverBuilder();
$moduleResolver = $moduleResolver->build(
    require APP_FOLDER . 'config/module-resolver-config.php',
    require APP_FOLDER . 'config/local-config.php',
    APP_FOLDER,
    'config',
    ENV
);

$moduleFolder = $moduleResolver->getModuleFolder();
$definitions = $moduleResolver->getDefinitions();
$localValue = $moduleResolver->getLocalValue();

$builder = new ContainerBuilder();
$builder->useAutowiring(true);
$builder->useAnnotations(false);
$builder->addDefinitions($definitions);
$container = $builder->build();
$container->set('local', $localValue);

$loggerFactory = new LoggerFactory([
    'path' => ROOT_FOLDER . 'var/logs',
    'level' => Logger::DEBUG,
    'file_permission' => 0777,
]);
$loggerApp = $loggerFactory
    ->addFileHandler('app.log')
    ->addConsoleHandler()
    ->createInstance('app');
$container->set(LoggerFactory::class, $loggerFactory);
$container->set('logger_app', $loggerApp);

$app = $container->get(App::class);

require_once $moduleFolder . 'bootstrap/middleware.php';
require_once $moduleFolder . 'bootstrap/routes.php';

$app->run();
