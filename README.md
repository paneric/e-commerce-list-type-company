# BASE MICRO SERVICE

## Clone repository

```sh
$ git clone https://paneric@bitbucket.org/paneric/base-micro-service.git
$ mv base-micro-service e-commerce-list-type-company
```

## Set a custom module

Adapt project `name` and `psr-4`:

*composer.json*
```json
{
    "name": "paneric/e-commerce-list-type-company",
    ...
    "autoload": {
        "psr-4": {
            "ECommerce\\ListTypeCompany\\": "src"
        }
    },
    ...
}
```

Run composer:

```sh
$ composer update
```

Make sure the `scope` variable is set as `lib`:

*bin/app*
```php
#!/usr/bin/env php
<?php

$scope = 'lib';
...
```
Run console command:

```sh
$ bin/app bms

Psr-4 (no trailing backslash !!!): ECommerce
Vendor service folder path (relative to project folder, no trailing slash !!!): vendor/paneric/e-commerce-list-type-company
Service name (CamelCase !!!): ListTypeCompany
DB table column name prefix (lower case !!!): ltc
DAO attributes names (camelCase, coma separated !!!): ref,pl,en
DAO attributes types (CamelCase, coma separated !!!): string,string,string
DAO unique attribute name (camelCase !!!): ref

                                                                          
  BASE MICRO SERVICE UPDATE SUCCESS:                                      
                                                                          

  Resources update with vendor "ECommerce", service "ListTypeCompany" and prefix "ltc" success.
```

## Environment variables
*.env*
```
ENV = 'dev'
APP_DOMAIN = '127.0.0.1:8080'
BASE_API_URL = 'http://127.0.0.1:8081'

# GUARD:
KEY_ASCII = 'def000008d147968b763eac2453624272c423c77347bd423e736328d648365fb243ea2130953f36ff38b5c9d8b15e5063df9e014010a3e06a1ec60612e54737a9f54f45d'
# DB:
DB_HOST = 'localhost'
DB_NAME = 'e_commerce'
DB_USR = 'toor'
DB_PSWD = 'toor'
JWT_SECRET = 'NRA4kcEMGUr+w5zOWpsVJ7v4NmQVVAuMZfZFll5g4pjnTGHJFj8Fw24bvUlRsttJrY0/ZBtav66nCzuD0S/rrBuNRGsV8QkulREVa9krRxTO/mnx1fgVIPBMacHMm6hN'
```

## Migrations and fixtures

#### Install migrations
```sh
$ composer require --dev robmorgan/phinx
```
#### Create migrations and seed folders
```sh
$ mkdir db
$ cd db
$ mkdir migrations
$ mkdir seeds
```
#### Init migrations settings
```sh
$ bin/phinx init
```
> That will create `phinx.php` file in your root directory. Update your db credentials.

*phinx.php*
```php
return
[
    ...
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_environment' => 'development',
        'production' => [
            'adapter' => 'mysql',
            'host' => 'localhost',
            'name' => 'e_commerce',
            'user' => 'toor',
            'pass' => 'toor',
            'port' => '3306',
            'charset' => 'utf8',
        ],
        'development' => [
            ...
        ],
        'testing' => [
            ...
        ]
    ],
    'version_order' => 'creation'
];
```

#### [Create your migration](https://book.cakephp.org/phinx/0/en/migrations.html)
```sh
$ bin/phinx create ListTypeCompanyMigration
```

*db/migrations/20210208151830_list-type-company_migration.php*
```php
use Phinx\Migration\AbstractMigration;

final class ListTypeCompanyMigration extends AbstractMigration
{
    public function change(): void
    {
        ...
    }
}
```

#### [Create your fixtures](https://book.cakephp.org/phinx/0/en/migrations.html)
```sh
$ composer require --dev fzaninotto/faker
```
```sh
$ bin/phinx seed:create ListTypeCompanySeeder
```

*db/seeds/ListTypeCompanySeeder.php*
```php
use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class ListTypeCompanySeeder extends AbstractSeed
{
    public function run()
    {
        ...
    }
}
```

#### Run migrations (standard)
```sh
$ bin/phinx migrate -e development
```
#### Run migrations (Docker)
```sh
$ sudo docker exec 57f bin/phinx migrate -e development  
```

#### Run fixtures (standard)
```sh
$ bin/phinx seed:run -e development
```
#### Run fixtures (Docker)
```sh
$ sudo docker exec 57f bin/phinx seed:run -e development
```
#### [Migrations and fixtures commands](https://book.cakephp.org/phinx/0/en/commands.html)


## Apache

Create log folder:  
```sh
$ mkdir var
$ mkdir var/cache
$ mkdir var/logs
$ sudo chmod -R 777 var
```

*/etc/apache2/sites-available*
```sh
$ cp 000-default.conf e-commerce.conf
```

*/etc/apache2/sites-available/e-commerce.conf*
```
Listen 8080
Listen 8081

<VirtualHost 127.0.0.1:8080>
    ServerAdmin webmaster@e-commerce.test
    ServerName e-commerce.test
    ServerAlias www.e-commerce.test

    DocumentRoot /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-list-type-company/public
    
    <Directory /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-list-type-company/public/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-list-type-company/var/apache_error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

<VirtualHost 127.0.0.1:8081>
    ServerAdmin webmaster@e-commerce.test
    ServerName e-commerce.test
    ServerAlias www.e-commerce.test

    DocumentRoot /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-list-type-company/public/
    
    <Directory /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-list-type-company/public/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-list-type-company/var/apache_error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

*/etc/apache2/sites-available*
```
$ sudo a2ensite e-commerce.conf  
$ sudo a2dissite 000-default.conf
```


Ensure that the Apache mod_rewrite module is installed and enabled. In order to enable mod_rewrite you can type the following command in the terminal:
```sh
$ sudo a2enmod rewrite
$ sudo a2enmod actions
```

Restart Apache:
```sh
$ sudo systemctl restart apache2  
$ systemctl status apache2.service
$ sudo apachectl configtest
```

*e-commerce-list-type-company/.htaccess*
```
RewriteEngine On
RewriteCond %{REQUEST_URI} !^/public
RewriteRule ^(.*)$ /public/$1 [NC,L]

RewriteRule .* - [env=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
```

*e-commerce-list-type-company/public/.htaccess*
```
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^ index.php [QSA,L]

RewriteRule .* - [env=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
```