<?php

declare(strict_types=1);

use ECommerce\ListTypeCompany\Gateway\ListTypeCompanyDTO;

return [

    'validation' => [

        'api-ltc.create' => [
            'methods' => ['POST'],
            ListTypeCompanyDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'api-ltcs.create' => [
            'methods' => ['POST'],
            ListTypeCompanyDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'api-ltc.update' => [
            'methods' => ['PUT'],
            ListTypeCompanyDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'api-ltcs.update' => [
            'methods' => ['PUT'],
            ListTypeCompanyDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'api-ltcs.delete' => [
            'methods' => ['POST'],
            ListTypeCompanyDTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ]
                ],
            ],
        ],
    ]
];
