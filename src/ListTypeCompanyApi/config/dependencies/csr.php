<?php

use ECommerce\ListTypeCompany\ListTypeCompanyApi\config\ListTypeCompanyApiActionConfig;
use ECommerce\ListTypeCompany\Repository\ListTypeCompanyRepositoryInterface;
use Paneric\ComponentModule\Module\Action\Api\CreateApiAction;
use Paneric\ComponentModule\Module\Action\Api\CreateMultipleApiAction;
use Paneric\ComponentModule\Module\Action\Api\DeleteApiAction;
use Paneric\ComponentModule\Module\Action\Api\DeleteMultipleApiAction;
use Paneric\ComponentModule\Module\Action\Api\GetAllApiAction;
use Paneric\ComponentModule\Module\Action\Api\GetAllPaginatedApiAction;
use Paneric\ComponentModule\Module\Action\Api\GetOneByIdApiAction;
use Paneric\ComponentModule\Module\Action\Api\UpdateApiAction;
use Paneric\ComponentModule\Module\Action\Api\UpdateMultipleApiAction;
use Psr\Container\ContainerInterface;

return [

    'list_type_company_create_api_action' => static function (ContainerInterface $container): CreateApiAction
    {
        return new CreateApiAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(ListTypeCompanyApiActionConfig::class),
        );
    },

    'list_type_company_create_multiple_api_action' => static function (ContainerInterface $container): CreateMultipleApiAction
    {
        return new CreateMultipleApiAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(ListTypeCompanyApiActionConfig::class),
        );
    },

    'list_type_company_delete_api_action' => static function (ContainerInterface $container): DeleteApiAction
    {
        return new DeleteApiAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(ListTypeCompanyApiActionConfig::class),
        );
    },

    'list_type_company_delete_multiple_api_action' => static function (ContainerInterface $container): DeleteMultipleApiAction
    {
        return new DeleteMultipleApiAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(ListTypeCompanyApiActionConfig::class),
        );
    },

    'list_type_company_get_all_api_action' => static function (ContainerInterface $container): GetAllApiAction
    {
        return new GetAllApiAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(ListTypeCompanyApiActionConfig::class),
        );
    },

    'list_type_company_get_all_paginated_api_action' => static function (ContainerInterface $container): GetAllPaginatedApiAction
    {
        return new GetAllPaginatedApiAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(ListTypeCompanyApiActionConfig::class),
        );
    },

    'list_type_company_get_one_by_id_api_action' => static function (ContainerInterface $container): GetOneByIdApiAction
    {
        return new GetOneByIdApiAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(ListTypeCompanyApiActionConfig::class),
        );
    },

    'list_type_company_update_api_action' => static function (ContainerInterface $container): UpdateApiAction
    {
        return new UpdateApiAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(ListTypeCompanyApiActionConfig::class),
        );
    },

    'list_type_company_update_multiple_api_action' => static function (ContainerInterface $container): UpdateMultipleApiAction
    {
        return new UpdateMultipleApiAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(ListTypeCompanyApiActionConfig::class),
        );
    },
];
