<?php

declare(strict_types=1);

use ECommerce\ListTypeCompany\config\ListTypeCompanyRepositoryConfig;
use ECommerce\ListTypeCompany\Repository\ListTypeCompanyRepository;
use ECommerce\ListTypeCompany\Repository\ListTypeCompanyRepositoryInterface;
use Paneric\DBAL\Manager;
use Psr\Container\ContainerInterface;

return [
    ListTypeCompanyRepositoryInterface::class => static function (ContainerInterface $container): ListTypeCompanyRepository {
        return new ListTypeCompanyRepository(
            $container->get(Manager::class),
            $container->get(ListTypeCompanyRepositoryConfig::class)
        );
    },
];
