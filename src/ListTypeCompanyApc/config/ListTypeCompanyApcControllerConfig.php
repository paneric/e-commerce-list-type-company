<?php

declare(strict_types=1);

namespace ECommerce\ListTypeCompany\ListTypeCompanyApc\config;

use Paneric\Interfaces\Config\ConfigInterface;

class ListTypeCompanyApcControllerConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'route_prefix' => 'ltc'
        ];
    }
}
