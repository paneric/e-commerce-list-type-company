<?php

declare(strict_types=1);

namespace ECommerce\ListTypeCompany\ListTypeCompanyApc\Controller;

use Paneric\ComponentModule\Module\Controller\Base\BaseModuleApcController;

class ListTypeCompanyApcController extends BaseModuleApcController {}
