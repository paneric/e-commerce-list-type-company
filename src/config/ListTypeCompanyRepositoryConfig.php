<?php

declare(strict_types=1);

namespace ECommerce\ListTypeCompany\config;

use ECommerce\ListTypeCompany\Gateway\ListTypeCompanyDAO;
use Paneric\Interfaces\Config\ConfigInterface;
use PDO;

class ListTypeCompanyRepositoryConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'table' => 'list_type_companys',
            'dao_class' => ListTypeCompanyDAO::class,
            'fetch_mode' => PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,
            'create_unique_where' => sprintf(
                ' %s',
                'WHERE ltc_ref=:ltc_ref'
            ),
            'update_unique_where' => sprintf(
                ' %s %s',
                'WHERE ltc_ref=:ltc_ref',
                'AND ltc_id NOT IN (:ltc_id)'
            ),
        ];
    }
}
