<?php

declare(strict_types=1);

namespace ECommerce\ListTypeCompany\Repository;

use Paneric\ComponentModule\Module\Repository\ModuleRepository;

class ListTypeCompanyRepository extends ModuleRepository implements ListTypeCompanyRepositoryInterface {}
