<?php

declare(strict_types=1);

namespace ECommerce\ListTypeCompany\ListTypeCompanyApp\config;

use Paneric\Interfaces\Config\ConfigInterface;

class ListTypeCompanyAppControllerConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'route_prefix' => 'ltc'
        ];
    }
}
