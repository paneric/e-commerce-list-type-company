<?php

declare(strict_types=1);

use ECommerce\ListTypeCompany\ListTypeCompanyApi\Controller\ListTypeCompanyApiController;
use Paneric\Pagination\PaginationMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Tuupola\Middleware\JwtAuthentication;

if (isset($app, $container)) {

    $app->map(['GET'], '/api-ltc/get/{id}', function(Request $request, Response $response, array $args){
        return $this->get(ListTypeCompanyApiController::class)->getOneById(
            $request,
            $response,
            $this->get('list_type_company_get_one_by_id_api_action'),
            $args['id']
        );
    })->setName('api-ltc.get')
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->map(['GET'], '/api-ltcs/get', function(Request $request, Response $response){
        return $this->get(ListTypeCompanyApiController::class)->getAll(
            $request,
            $response,
            $this->get('list_type_company_get_all_api_action')
        );
    })->setName('api-ltcs.get')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->map(['GET'], '/api-ltcs/get/{page}', function(Request $request, Response $response, array $args){
        return $this->get(ListTypeCompanyApiController::class)->getAllPaginated(
            $request,
            $response,
            $this->get('list_type_company_get_all_paginated_api_action'),
            $args['page']
        );
    })->setName('api-ltcs.get.page')
        ->addMiddleware($container->get(PaginationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->post('/api-ltc/create', function(Request $request, Response $response){
        return $this->get(ListTypeCompanyApiController::class)->create(
            $request,
            $response,
            $this->get('list_type_company_create_api_action')
        );
    })->setName('api-ltc.create')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->post('/api-ltcs/create', function(Request $request, Response $response){
        return $this->get(ListTypeCompanyApiController::class)->createMultiple(
            $request,
            $response,
            $this->get('list_type_company_create_multiple_api_action')
        );
    })->setName('api-ltcs.create')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->put('/api-ltc/update/{id}', function(Request $request, Response $response, array $args){
        return $this->get(ListTypeCompanyApiController::class)->update(
            $request,
            $response,
            $this->get('list_type_company_update_api_action'),
            $args['id']
        );
    })->setName('api-ltc.update')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->put('/api-ltcs/update', function(Request $request, Response $response){
        return $this->get(ListTypeCompanyApiController::class)->updateMultiple(
            $request,
            $response,
            $this->get('list_type_company_update_multiple_api_action')
        );
    })->setName('api-ltcs.update')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));


    $app->delete('/api-ltc/delete/{id}', function(Request $request, Response $response, array $args){
        return $this->get(ListTypeCompanyApiController::class)->delete(
            $request,
            $response,
            $this->get('list_type_company_delete_api_action'),
            $args['id']
        );
    })->setName('api-ltc.delete')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->post('/api-ltcs/delete', function(Request $request, Response $response){
        return $this->get(ListTypeCompanyApiController::class)->deleteMultiple(
            $request,
            $response,
            $this->get('list_type_company_delete_multiple_api_action')
        );
    })->setName('api-ltcs.delete')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));
}
