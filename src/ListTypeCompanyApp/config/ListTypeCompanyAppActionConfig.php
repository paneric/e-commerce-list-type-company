<?php

declare(strict_types=1);

namespace ECommerce\ListTypeCompany\ListTypeCompanyApp\config;

use ECommerce\ListTypeCompany\Gateway\ListTypeCompanyDAO;
use ECommerce\ListTypeCompany\Gateway\ListTypeCompanyDTO;
use Paneric\Interfaces\Config\ConfigInterface;

class ListTypeCompanyAppActionConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'get_one_by_id' => [
                'module_name_sc' => 'list_type_company',
                'prefix' => 'ltc',
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['ltc_id' => (int) $id];
                },
            ],

            'get_all' => [
                'module_name_sc' => 'list_type_company',
                'prefix' => 'ltc',
                'order_by' => static function (string $local = null): array
                {
                    return [];
                },
            ],

            'get_all_paginated' => [
                'module_name_sc' => 'list_type_company',
                'prefix' => 'ltc',
                'find_by_criteria' => static function (string $local = null): array
                {
                    return [];
                },
                'order_by' => static function (string $local): array
                {
                    return [];
                },
            ],

            'create' => [
                'module_name_sc' => 'list_type_company',
                'prefix' => 'ltc',
                'dao_class' => ListTypeCompanyDAO::class,
                'dto_class' => ListTypeCompanyDTO::class,
                'create_unique_criteria' => static function (array $attributes): array
                {
                    return ['ltc_ref' => $attributes['ref']];
                },
            ],

            'create_multiple' => [
                'module_name_sc' => 'list_type_company',
                'prefix' => 'ltc',
                'dao_class' => ListTypeCompanyDAO::class,
                'dto_class' => ListTypeCompanyDTO::class,
                'create_unique_criteria' => static function (array $collection): array
                {
                    $createUniqueCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $createUniqueCriteria[$index] = ['ltc_ref' => $dao->getRef()];
                    }

                    return $createUniqueCriteria;
                },
            ],

            'update' => [
                'module_name_sc' => 'list_type_company',
                'prefix' => 'ltc',
                'dao_class' => ListTypeCompanyDAO::class,
                'dto_class' => ListTypeCompanyDTO::class,
                'find_one_by_criteria' => static function (ListTypeCompanyDAO $dao, string $id): array
                {
                    return ['ltc_id' => (int) $id, 'ltc_ref' => $dao->getRef()];
                },
                'update_unique_criteria' => static function (string $id): array
                {
                    return ['ltc_id' => (int) $id];
                },
            ],

            'update_multiple' => [
                'module_name_sc' => 'list_type_company',
                'prefix' => 'ltc',
                'dao_class' => ListTypeCompanyDAO::class,
                'dto_class' => ListTypeCompanyDTO::class,
                'find_by_criteria' => static function (array $collection): array
                {
                    $findByCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $findByCriteria[$index] = [
                            'ltc_id' => (int) $index,
                            'ltc_ref' => $dao->getRef(),
                        ];
                    }

                    return $findByCriteria;
                },

                'update_unique_criteria' => static function (array $collection): array
                {
                    $updateUniqueCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $updateUniqueCriteria[$index] = [
                            'ltc_id' => (int) $index,
                        ];
                    }

                    return $updateUniqueCriteria;
                },
            ],

            'delete' => [
                'module_name_sc' => 'list_type_company',
                'delete_by_criteria' => static function (array $attributes): array
                {
                    $deleteByCriteria = [];

                    foreach ($attributes as $key => $value) {
                        $deleteByCriteria['ltc_' . $key] = (int) $value;
                    }

                    return $deleteByCriteria;
                },
            ],

            'delete_multiple' => [
                'module_name_sc' => 'list_type_company',
                'dao_class' => ListTypeCompanyDAO::class,
                'dto_class' => ListTypeCompanyDTO::class,
                'delete_by_criteria' => static function (array $daoCollection): array
                {
                    $deleteByCriteria = [];

                    foreach ($daoCollection as $index => $dao) {
                            $deleteByCriteria[$index]['ltc_id'] = (int) $dao->getId();

                    }

                    return $deleteByCriteria;
                },
            ],
        ];
    }
}
