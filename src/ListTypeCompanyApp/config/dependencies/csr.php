<?php

use ECommerce\ListTypeCompany\ListTypeCompanyApp\config\ListTypeCompanyAppActionConfig;
use ECommerce\ListTypeCompany\ListTypeCompanyApp\config\ListTypeCompanyAppControllerConfig;
use ECommerce\ListTypeCompany\ListTypeCompanyApp\Controller\ListTypeCompanyAppController;
use ECommerce\ListTypeCompany\Repository\ListTypeCompanyRepositoryInterface;
use Paneric\ComponentModule\Module\Action\App\CreateAppAction;
use Paneric\ComponentModule\Module\Action\App\CreateMultipleAppAction;
use Paneric\ComponentModule\Module\Action\App\DeleteAppAction;
use Paneric\ComponentModule\Module\Action\App\DeleteMultipleAppAction;
use Paneric\ComponentModule\Module\Action\App\GetAllAppAction;
use Paneric\ComponentModule\Module\Action\App\GetAllPaginatedAppAction;
use Paneric\ComponentModule\Module\Action\App\GetOneByIdAppAction;
use Paneric\ComponentModule\Module\Action\App\UpdateAppAction;
use Paneric\ComponentModule\Module\Action\App\UpdateMultipleAppAction;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Container\ContainerInterface;
use Twig\Environment as Twig;

return [

    ListTypeCompanyAppController::class => static function(ContainerInterface $container): ListTypeCompanyAppController
    {
        return new ListTypeCompanyAppController(
            $container->get(Twig::class),
            $container->get(ListTypeCompanyAppControllerConfig::class),
        );
    },

    'list_type_company_create_app_action' => static function (ContainerInterface $container): CreateAppAction
    {
        return new CreateAppAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListTypeCompanyAppActionConfig::class),
        );
    },

    'list_type_company_create_multiple_app_action' => static function (ContainerInterface $container): CreateMultipleAppAction
    {
        return new CreateMultipleAppAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListTypeCompanyAppActionConfig::class),
        );
    },

    'list_type_company_delete_app_action' => static function (ContainerInterface $container): DeleteAppAction
    {
        return new DeleteAppAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListTypeCompanyAppActionConfig::class),
        );
    },

    'list_type_company_delete_multiple_app_action' => static function (ContainerInterface $container): DeleteMultipleAppAction
    {
        return new DeleteMultipleAppAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListTypeCompanyAppActionConfig::class),
        );
    },

    'list_type_company_get_all_app_action' => static function (ContainerInterface $container): GetAllAppAction
    {
        return new GetAllAppAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListTypeCompanyAppActionConfig::class),
        );
    },

    'list_type_company_get_all_paginated_app_action' => static function (ContainerInterface $container): GetAllPaginatedAppAction
    {
        return new GetAllPaginatedAppAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListTypeCompanyAppActionConfig::class),
        );
    },

    'list_type_company_get_one_by_id_app_action' => static function (ContainerInterface $container): GetOneByIdAppAction
    {
        return new GetOneByIdAppAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListTypeCompanyAppActionConfig::class),
        );
    },

    'list_type_company_update_app_action' => static function (ContainerInterface $container): UpdateAppAction
    {
        return new UpdateAppAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListTypeCompanyAppActionConfig::class),
        );
    },

    'list_type_company_update_multiple_app_action' => static function (ContainerInterface $container): UpdateMultipleAppAction
    {
        return new UpdateMultipleAppAction (
            $container->get(ListTypeCompanyRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(ListTypeCompanyAppActionConfig::class),
        );
    },
];
