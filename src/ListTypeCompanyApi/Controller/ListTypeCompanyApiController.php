<?php

declare(strict_types=1);

namespace ECommerce\ListTypeCompany\ListTypeCompanyApi\Controller;

use Paneric\ComponentModule\Module\Controller\Base\BaseModuleApiController;

class ListTypeCompanyApiController extends BaseModuleApiController {}
