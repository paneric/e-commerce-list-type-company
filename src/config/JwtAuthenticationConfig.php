<?php

declare(strict_types=1);

namespace ECommerce\ListTypeCompany\config;

use Paneric\Interfaces\Config\ConfigInterface;

class JwtAuthenticationConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'secret' => $_ENV['JWT_SECRET'],
            'algorithm' => 'HS256',
            'secure' => false, // only for localhost for prod and test env set true
            'error' => static function ($response, $arguments) {
                $data['status'] = 401;
                $data['error'] = 'Unauthorized/'. $arguments['message'];
                return $response
                    ->withHeader('Content-Type', 'application/json;charset=utf-8')
                    ->getBody()->write(json_encode(
                        $data,
                        JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
                    ));
            }
        ];
    }
}
