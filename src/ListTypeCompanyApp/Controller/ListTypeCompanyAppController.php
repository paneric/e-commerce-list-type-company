<?php

declare(strict_types=1);

namespace ECommerce\ListTypeCompany\ListTypeCompanyApp\Controller;

use Paneric\ComponentModule\Module\Controller\Base\BaseModuleAppController;

class ListTypeCompanyAppController extends BaseModuleAppController {}
