<?php

declare(strict_types=1);

namespace ECommerce\ListTypeCompany\Repository;

use Paneric\ComponentModule\Interfaces\Repository\ModuleRepositoryInterface;

interface ListTypeCompanyRepositoryInterface extends ModuleRepositoryInterface
{}
