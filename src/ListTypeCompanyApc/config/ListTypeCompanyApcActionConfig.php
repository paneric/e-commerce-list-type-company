<?php

declare(strict_types=1);

namespace ECommerce\ListTypeCompany\ListTypeCompanyApc\config;

use Paneric\Interfaces\Config\ConfigInterface;
use ECommerce\ListTypeCompany\Gateway\ListTypeCompanyDAO;

class ListTypeCompanyApcActionConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        $apiEndpoints = [
            'base_url' => $_ENV['BASE_API_URL'],

            'api-prefix.get'       => '/api-ltc/get/',
            'api-prefixs.get'      => '/api-ltcs/get',
            'api-prefixs.get.page' => '/api-ltcs/get/',

            'api-prefix.create'    => '/api-ltc/create',
            'api-prefixs.create'   => '/api-ltcs/create',

            'api-prefix.update'    => '/api-ltc/update/',
            'api-prefixs.update'   => '/api-ltcs/update',

            'api-prefix.delete'    => '/api-ltc/delete/',
            'api-prefixs.delete'   => '/api-ltcs/delete',

            'apc-prefixs.show-all-paginated' => '/apc-ltcs/show-all-paginated/',
            'apc-prefixs.edit'               => '/apc-ltcs/edit/',
            'apc-prefixs.remove'             => '/apc-ltcs/remove/',
        ];

        return [
            'get_one_by_id' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_type_company',
                    'prefix' => 'ltc'
                ]
            ),

            'get_all' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_type_company',
                    'prefix' => 'ltc'
                ]
            ),

            'get_all_paginated' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_type_company',
                    'prefix' => 'ltc'
                ]
            ),


            'create' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_type_company',
                    'prefix' => 'ltc'
                ]
            ),

            'create_multiple' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_type_company',
                    'dao_class' => ListTypeCompanyDAO::class,
                    'prefix' => 'ltc'
                ]
            ),

            'update' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_type_company',
                    'prefix' => 'ltc'
                ]
            ),

            'update_multiple' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_type_company',
                ]
            ),

            'delete' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_type_company',
                ]
            ),

            'delete_multiple' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'list_type_company',
                ]
            ),
        ];
    }
}
