<?php

declare(strict_types=1);

use ECommerce\ListTypeCompany\Gateway\ListTypeCompanyDTO;

return [

    'validation' => [

        'ltc.add' => [
            'methods' => ['POST'],
            ListTypeCompanyDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'ltcs.add' => [
            'methods' => ['POST'],
            ListTypeCompanyDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'ltc.edit' => [
            'methods' => ['POST'],
            ListTypeCompanyDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'ltcs.edit' => [
            'methods' => ['POST'],
            ListTypeCompanyDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ], 
                    'pl' => [
                        'required' => [],
                    ], 
                    'en' => [
                        'required' => [],
                    ], 

                ],
            ],
        ],

        'ltc.remove' => [
            'methods' => ['POST'],
            ListTypeCompanyDTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ],
                ],
            ],
        ],

        'ltcs.remove' => [
            'methods' => ['POST'],
            ListTypeCompanyDTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ],
                ],
            ],
        ],
    ]
];
