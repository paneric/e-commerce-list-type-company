<?php

declare(strict_types=1);

use ECommerce\ListTypeCompany\ListTypeCompanyApp\Controller\ListTypeCompanyAppController;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Pagination\PaginationMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {

    $app->map(['GET'], '/ltc/show-one-by-id/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(ListTypeCompanyAppController::class)->showOneById(
            $response,
            $this->get('list_type_company_get_one_by_id_app_action'),
            $args['id'] ?? '1'
        );
    })->setName('ltc.show-one-by-id');


    $app->map(['GET'], '/ltcs/show-all', function(Request $request, Response $response) {
        return $this->get(ListTypeCompanyAppController::class)->showAll(
            $response,
            $this->get('list_type_company_get_all_app_action')
        );
    })->setName('ltcs.show-all');

    $app->map(['GET'], '/ltcs/show-all-paginated[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(ListTypeCompanyAppController::class)->showAllPaginated(
            $request,
            $response,
            $this->get('list_type_company_get_all_paginated_app_action'),
            $args['page'] ?? '1'
        );
    })->setName('ltcs.show-all-paginated')
        ->addMiddleware($container->get(PaginationMiddleware::class));


    $app->map(['GET', 'POST'], '/ltc/add', function(Request $request, Response $response) {
        return $this->get(ListTypeCompanyAppController::class)->add(
            $request,
            $response,
            $this->get('list_type_company_create_app_action')
        );
    })->setName('ltc.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/ltcs/add', function(Request $request, Response $response) {
        return $this->get(ListTypeCompanyAppController::class)->addMultiple(
            $request,
            $response,
            $this->get('list_type_company_create_multiple_app_action')
        );
    })->setName('ltcs.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));


    $app->map(['GET', 'POST'], '/ltc/edit/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(ListTypeCompanyAppController::class)->edit(
            $request,
            $response,
            $this->get('list_type_company_get_one_by_id_app_action'),
            $this->get('list_type_company_update_app_action'),
            $args['id']
        );
    })->setName('ltc.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/ltcs/edit[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(ListTypeCompanyAppController::class)->editMultiple(
            $request,
            $response,
            $this->get('list_type_company_get_all_paginated_app_action'),
            $this->get('list_type_company_update_multiple_app_action'),
            $args['page'] ?? '1'
        );
    })->setName('ltcs.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(PaginationMiddleware::class));


    $app->map(['GET', 'POST'], '/ltc/remove/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(ListTypeCompanyAppController::class)->remove(
            $request,
            $response,
            $this->get('list_type_company_delete_app_action'),
            $args['id']
        );
    })->setName('ltc.remove')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/ltcs/remove[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(ListTypeCompanyAppController::class)->removeMultiple(
            $request,
            $response,
            $this->get('list_type_company_get_all_paginated_app_action'),
            $this->get('list_type_company_delete_multiple_app_action'),
            $args['page'] ?? '1'
        );
    })->setName('ltcs.remove')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(PaginationMiddleware::class));
}
