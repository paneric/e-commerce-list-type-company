<?php

declare(strict_types=1);

return [
    'default_route_key' => 'ltc',
    'local_map' => ['en', 'pl'],
    'dash_as_slash' => false,
    'merge_module_cross_map' => false,
    'module_map' => [
        'ltc'      => ROOT_FOLDER . 'src/ListTypeCompanyApp',
        'ltcs'     => ROOT_FOLDER . 'src/ListTypeCompanyApp',
        'api-ltc'  => ROOT_FOLDER . 'src/ListTypeCompanyApi',
        'api-ltcs' => ROOT_FOLDER . 'src/ListTypeCompanyApi',
        'apc-ltc'  => ROOT_FOLDER . 'src/ListTypeCompanyApc',
        'apc-ltcs' => ROOT_FOLDER . 'src/ListTypeCompanyApc',
    ],
    'module_map_cross' => [
        'ltc'       => ROOT_FOLDER . 'vendor/paneric/e-commerce-list-type-company/src/ListTypeCompanyApp',
        'ltcs'      => ROOT_FOLDER . 'vendor/paneric/e-commerce-list-type-company/src/ListTypeCompanyApp',
        'api-ltc'   => ROOT_FOLDER . 'vendor/paneric/e-commerce-list-type-company/src/ListTypeCompanyApi',
        'api-ltcs'  => ROOT_FOLDER . 'vendor/paneric/e-commerce-list-type-company/src/ListTypeCompanyApi',
        'apc-ltc'   => ROOT_FOLDER . 'vendor/paneric/e-commerce-list-type-company/src/ListTypeCompanyApc',
        'apc-ltcs'  => ROOT_FOLDER . 'vendor/paneric/e-commerce-list-type-company/src/ListTypeCompanyApc',
    ],
];
