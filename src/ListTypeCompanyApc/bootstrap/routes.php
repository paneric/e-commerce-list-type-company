<?php

declare(strict_types=1);

use ECommerce\ListTypeCompany\ListTypeCompanyApc\Controller\ListTypeCompanyApcController;
use Paneric\Middleware\CSRFMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Paneric\Middleware\JWTAuthenticationEncoderMiddleware;

if (isset($app, $container)) {

    $app->map(['GET'], '/apc-ltc/show-one-by-id/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(ListTypeCompanyApcController::class)->showOneById(
            $request,
            $response,
            $this->get('list_type_company_get_one_by_id_apc_action'),
            $args['id'] ?? '1'
        );
    })->setName('apc-ltc.show-one-by-id')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));


    $app->map(['GET'],'/apc-ltcs/show-all', function(Request $request, Response $response) {
        return $this->get(ListTypeCompanyApcController::class)->showAll(
            $request,
            $response,
            $this->get('list_type_company_get_all_apc_action')
        );
    })->setName('apc-ltcs.show-all')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET'],'/apc-ltcs/show-all-paginated[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(ListTypeCompanyApcController::class)->showAllPaginated(
            $request,
            $response,
            $this->get('list_type_company_get_all_paginated_apc_action'),
            $args['page'] ?? '1'
        );
    })->setName('apc-ltcs.show-all-paginated')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class)); // no pagination middleware !!!


    $app->map(['GET', 'POST'], '/apc-ltc/add', function(Request $request, Response $response) {
        return $this->get(ListTypeCompanyApcController::class)->add(
            $request,
            $response,
            $this->get('list_type_company_create_apc_action')
        );
    })->setName('apc-ltc.add')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-ltcs/add', function(Request $request, Response $response) {
        return $this->get(ListTypeCompanyApcController::class)->addMultiple(
            $request,
            $response,
            $this->get('list_type_company_create_multiple_apc_action')
        );
    })->setName('apc-ltcs.add')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));


    $app->map(['GET', 'POST'], '/apc-ltc/edit/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(ListTypeCompanyApcController::class)->edit(
            $request,
            $response,
            $this->get('list_type_company_get_one_by_id_apc_action'),
            $this->get('list_type_company_update_apc_action'),
            $args['id']
        );
    })->setName('apc-ltc.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-ltcs/edit[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(ListTypeCompanyApcController::class)->editMultiple(
            $request,
            $response,
            $this->get('list_type_company_get_all_paginated_apc_action'),
            $this->get('list_type_company_update_multiple_apc_action'),
            $args['page'] ?? '1'
        );
    })->setName('apc-ltcs.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));


    $app->map(['GET', 'POST'], '/apc-ltc/remove/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(ListTypeCompanyApcController::class)->remove(
            $request,
            $response,
            $this->get('list_type_company_delete_apc_action'),
            $args['id']
        );
    })->setName('apc-ltc.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-ltcs/remove[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(ListTypeCompanyApcController::class)->removeMultiple(
            $request,
            $response,
            $this->get('list_type_company_get_all_paginated_apc_action'),
            $this->get('list_type_company_delete_multiple_apc_action'),
            $args['page'] ?? '1'
        );
    })->setName('apc-ltcs.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));
}
